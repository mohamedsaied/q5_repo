/*
 * FreeRTOS_TaskCreation.c
 *
 * Created: 10/19/2016 12:12:58 AM
 * Author : Master
 */ 

#include "main.h"
#include <util/delay.h>
#include <avr/io.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "keypad.h"
#include "LCD_Lib.h"
#include <string.h>
#include "app_tasks.h"


extern xQueueHandle LCD_Queue;
extern xSemaphoreHandle CheckPasswordSemaphore;

int main()
{
	LCD_Initialization();
	LCD_Write_String("Hello");
	LCD_Queue = xQueueCreate(10,sizeof(char));
	CheckPasswordSemaphore = xSemaphoreCreateBinary();
	xTaskCreate( vTask_LCD, "LCD",200,NULL, 1, NULL );
	xTaskCreate( vTask_KeyPad, "Keypad",200,NULL, 2, NULL );
	xTaskCreate( vTask_Password_Check, "check",200,NULL, 3, NULL );
	vTaskStartScheduler();
	while(1);
}
