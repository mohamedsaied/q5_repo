/*
 * adc.c
 *
 * Created: 10/4/2019 8:37:00 PM
 *  Author: Mohamed
 */ 

#include "adc.h"
#include <avr/io.h>


void ADC_Init(){
	
	//Select Channel ADC1
	//Select REF Internal REF1:1 REF0 1
	//ADLAR (left or right adjust) 
	//ADMUX = 0b1100 0001
	//ADMUX = 0b11000001;
	DDRA &=~(1<<1); //ADC1 as input
	ADMUX |=(1<<REF0)|(1<<REF1)|(1<<ADC1);
	
	//ADEN Enable ADC
	//prescalar clk/128
	ADCSRA|=(1<<ADEN)|(1<<ADPS2)|(1<<ADPS1) |(1<<ADPS0);
	
}
//function Read
unsigned int ADC_Read(unsigned char channel){
	volatile unsigned int Converted_Value=0;
	
	//select channel
	ADMUX |= (ADMUX & 0x1F) | channel;
	//Start Conversion
	ADCSRA |=(1<<ADSC);
	//check flag
	while(!ADIF); //polling
	ADCSRA |= (1<<ADIF); //clear interrupt flag
	//read data register
	Converted_Value=ADCH<<8; //
	Converted_Value|=ADCL;
	
	return Converted_Value;
	
}
