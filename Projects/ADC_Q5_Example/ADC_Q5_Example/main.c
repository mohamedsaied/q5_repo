/*
 * ADC_Q5_Example.c
 *
 * Created: 10/4/2019 8:32:46 PM
 * Author : Mohamed
 */ 
/*
*Read POT value which ADC1
*Display the converted value on LCD
*/
//#define F_CPU	16000000
#include <avr/io.h>
#include "adc.h"
#include "lcd.h"
#include <stdlib.h>
 int value_pot;
 char value_arr[10];
int main(void)
{
	//intialise modules
	ADC_Init();
	LCD_Init(); //
    while (1) 
    {
		//read adc
		value_pot=ADC_Read(ADC1_channel);
		//convert to string 
		itoa(value_pot,value_arr,10);
		LCD_String(value_arr);
		_delay_ms(100);
    }
}

