/*
 * adc.h
 *
 * Created: 10/4/2019 8:37:11 PM
 *  Author: Mohamed
 */ 


#ifndef ADC_H_
#define ADC_H_
#define REF0	6
#define REF1	7
#define ADC1	0 //
#define ADC1_channel	1


void ADC_Init(void);
unsigned int ADC_Read(unsigned char channel); 


#endif /* ADC_H_ */